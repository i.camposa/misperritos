from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    
    path('',views.index,name='index'),
    path('Registrarse/',views.Registrarse,name="Registrarse"),
    path('Registrarse/CrearUsuarios',views.CrearUsuarios,name="CrearUsuarios"),
    path('CrearPerros/AgregarPerros',views.AgregarPerrosx,name="AgregarPerros"),
    path('CrearPerros/',views.CrearPerros,name="CrearPerros"),
    path('login/',views.login,name="login"),
    path('login/iniciarSesion',views.iniciarSesion,name="iniciarSesion"),
    path('Veditarperros/',views.Veditarperros,name="veditarperros"),
    path('Veditarperros/EditarPerros',views.EditarPerros,name="Editarperros")


 ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)