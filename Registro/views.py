from django.shortcuts import render , redirect
from django.http import HttpResponse
from .models import Adoptante , AgregarPerros
from django.contrib.auth.decorators import login_required
import datetime
from django.contrib.auth import authenticate
from django.contrib.auth.models import User

# Create your views here.
def index(request):
    usuario = request.session.get('usuario',None)
    return render(request,'index.html',{})

def Registrarse(request):
    return render(request,'Registrarse.html',{}) 

def login(request):
    return render(request,'login.html',{}) 

def CrearPerros(request):
    return render(request,'AgregarPerros.html',{}) 
    



def iniciarSesion(request):
    usuario = request.POST.get('correo','')
    claveUsu = request.POST.get('claveUsu','')
    
    user = authenticate(username=usuario, password=claveUsu)

    if user is not None:
        request.session['usuario'] = user.first_name+""+user.last_name
        return redirect("index")
    else:
        return redirect("login")


def CrearUsuarios(request):
    
    correo = request.POST.get('correo','')
    nombre = request.POST.get('nombre','')
    run = request.POST.get('run','')
    claveUsu = request.POST.get('claveUsu','')
    fechaNac = request.POST.get('fechaNac',18)
    fono = request.POST.get('fono','')
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    tipoCasa = request.POST.get('tipoCasa','')
    adoptante = Adoptante(correo=correo,nombre=nombre,run=run,claveUsu=claveUsu,fechaNac=fechaNac,fono=fono,region=region,comuna=comuna,tipoCasa=tipoCasa)
    adoptante.save()
    return redirect('index')

# @login_required(login_url='login') 
def AgregarPerrosx(request):
    nombre = request.POST.get('nombre','')
    raza = request.POST.get('raza','')
    descripcion = request.POST.get('descripcion','')
    Estado = request.POST.get('Estado','')
    foto = request.FILES.get('foto', False)
    agregarPerros = AgregarPerros(nombre=nombre,raza=raza,descripcion=descripcion,Estado=Estado, foto = foto)
    agregarPerros.save()
    return redirect('index')

# @login_required(login_url='login')    
def EliminarPerros(request):
    agregarPerros = AgregarPerros.objects.get(pk = id)
    agregarPerros.delete()
    return redirect('index')

def Veditarperros(request,id):
     agregarPerros = AgregarPerros.objects.get(pk = id)
     return render(request,'Veditarperros.html',{})

# @login_required(login_url='login')    
def EditarPerros(request):
    agregarPerros = AgregarPerros.objects.get(pk = id)
    foto = request.FILES.get('foto', False)
    nombre = request.POST.get('nombre','' )
    raza = request.POST.get('raza','' )
    descripcion = request.POST.get('descripcion','' )
    Estado = request.POST.get('Estado','' )
    id = request.POST.get('id',0)
    agregarPerros = AgregarPerros.objects.get(pk = id)
    agregarPerros.foto = foto
    agregarPerros.nombre = nombre
    agregarPerros.raza = raza
    agregarPerros.descripcion = descripcion
    agregarPerros.Estado = Estado
    agregarPerros.save()
    return redirect('index')

    


