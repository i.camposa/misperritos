from django.db import models


# Create your models here.

class Adoptante(models.Model):
    correo = models.EmailField(max_length=254)
    nombre = models.CharField(max_length=100)
    run = models.CharField(max_length=100) 
    claveUsu = models.CharField(max_length=40) 
    fechaNac = models.DateField() 
    fono = models.IntegerField()  
    region = models.CharField(max_length=100)
    comuna = models.CharField(max_length=100)
    tipoCasa = models.CharField(max_length=100)

    def __str__(self):
        return "Correo: "+self.correo+" Nombre: "+self.nombre+" Run: "+self.run+" Contraseña: "+self.claveUsu

class AgregarPerros(models.Model):
    
    nombre = models.CharField(max_length=100)
    raza = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)
    Estado = models.CharField(max_length=100)
    foto = models.ImageField(upload_to="img/")

    def __str__(self):
        return """ nombre {} 
                    raza {}
                    descr {} 
                    estado {} """.format(self.nombre,self.raza,self.descripcion,self.Estado)
