# Generated by Django 2.1.2 on 2018-11-03 02:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Registro', '0003_adoptante_claveusu'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgregarPerros',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('foto', models.ImageField(upload_to='img/')),
                ('nombre', models.CharField(max_length=100)),
                ('raza', models.CharField(max_length=100)),
                ('descripcion', models.CharField(max_length=100)),
                ('Estado', models.CharField(max_length=100)),
            ],
        ),
    ]
